Windows Audit Tool
==================

The extra 7z module was obtained from https://www.7-zip.org/sdk.html
and extracted to the folder `7z-extra/`

To create a self extracting exe, run:
```
./build-exe.sh
```

## Upload
To make this available at the webpage <https://www.ch.cam.ac.uk/computing/audit-tool-self-managed-computers>:

Either use this link <https://www.ch.cam.ac.uk/computing/index.php?q=elfinder&app=ckeditor> and skip to step 5 or start from the top:

1. First, navigate to that webpage and enter Edit mode
2. Select the link to download the audit tool
3. Left-click on the link. Then right-click on the link and choose "Edit link"
4. Then click "Browse server"
5. Navigate to the "audit-tool" folder
6. Choose the "Upload files" action (either from the toolbar or context menu); it looks like a floppy disk icon with a green plus on it
7. Select "Browse" and then select the updated `audit.exe`
8. OK!
9. Exit out of the link change dialog
