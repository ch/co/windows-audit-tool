<#
.DESCRIPTION
Audit an unmanaged computer.

.EXAMPLE
$0 -hostname "" -allow_my_ip_lookup:$False -sections "x,y" -no_submit

.EXAMPLE
powershell -ExecutionPolicy ByPass ./xymon_query_hostname_and_report.ps1 -hostname build1.ch.private.cam.ac.uk -allow_my_ip_lookup:$False -sections osver,software
#>
[CmdletBinding()]
param ([string]$hostname='', [switch]$allow_my_ip_lookup=$True, [switch]$do_xymon_check=$True, [string]$sections="all", [switch]$prompt_verify_datetime=$True, [switch]$show_all=$False, [switch]$no_submit=$False, [switch]$save_offline=$False, [switch]$help=$false, [switch]$h=$false, $xymonServerAddress = "131.111.112.27", $xymonServerPort = 1984
)

Set-StrictMode -Version Latest

$ErrorActionPreference = 'stop'
$IP_lookup_service = "myip.ch.cam.ac.uk"


###### following is Xymon.ps1
## can't source it, since then we couldn't extend it
## (unless we defined everything as PowerShell Modules)
<#
.Description
Talk to a Xymon server with XymonCommunicate.

Send a Xymon report with XymonReport.
#>

Set-StrictMode -Version Latest

Function XymonCommunicate {
    param ($message)

    # Connect to xymon
    $socket = new-object System.Net.Sockets.TcpClient($xymonServerAddress, $xymonServerPort)

    # Prepare the data
    $data = [System.Text.Encoding]::ASCII.GetBytes($message)

    # Send the data
    $myNetworkStream = $socket.GetStream()
    $myNetworkStream.Write($data, 0, $data.Length)

    # Signal that we've finished sending
    $socket.client.shutdown(1) | out-null

    # Receive the response
    $buffer_length = 1024
    $myReadBuffer = New-Object System.Byte[] $buffer_length
    $encoding = New-Object System.Text.AsciiEncoding

    $myCompleteMessage = [System.Text.StringBuilder]::new()

    # Incoming message may be larger than the buffer size, so repeat while data available.
    do {
        $numberOfBytesRead = $myNetworkStream.Read($myReadBuffer, 0, $myReadBuffer.Length)
        $myCompleteMessage.AppendFormat("{0}", $encoding.GetString($myReadBuffer, 0, $numberOfBytesRead)) | out-null

        # give the buffer a chance to fill before we bail
        # xymon protocol doesn't tell us how many bytes we should expect in the reply
        # so there is really no good way to check that we've received everything!
        # set a modest timeout instead but don't sleep unnecessarily long!
        # if we haven't had any further response within 10 milliseconds,
        # consider that we've seen the end of the message
        $waited = 0
        while ((-not $myNetworkStream.DataAvailable) -And ($waited -lt 10)) {
            Start-Sleep -Milliseconds 1
            $waited += 1
        }
    } while($myNetworkStream.DataAvailable)

    # Close socket
    $socket.close()

    #Write-Host ("message: $($myCompleteMessage.ToString())" | Format-Hex)

    Return($myCompleteMessage.ToString())
}

Function XymonCommunicateDebug {
    param ($message,
    $response_color = '[dynamic]',
    $request_lines_prefix = '  >>',
    $response_lines_prefix = '  <<'
    )

    $msg_padded = $Message -Split "`n"
    $msg_padded = $msg_padded -Join ("`n" + $request_lines_prefix)
    Write-Host "  Request to xymon ($($Message.Length) bytes):"
    Write-Host "$($request_lines_prefix)$msg_padded"

    $response = XymonCommunicate($Message)

    If ($response.Length -eq 0) {
        Write-Host "  Response from xymon was 0 bytes" -ForegroundColor yellow
    } Else {
        Write-Host "  Response from xymon ($($response.Length) bytes):"
        $response_padded = $response -Split "`n"
        $response_padded = $response_padded -Join ("`n" + $response_lines_prefix)

        $my_response_color = $Null
        Try {
            If (($response_color -eq "[dynamic]")) {
                $match = ($response -match "^(red|green|blue|yellow) ")
                If ($match) {
                    $my_response_color = $Matches[1]
                } Else {
                    $my_response_color = $Null
                }
            } Else {
                $my_response_color = $response_color
            }
        } Catch {}
        $print = "$($response_lines_prefix)$response_padded" | Format-Hex
        If (-Not (($my_response_color -eq $Null) -Or ($my_response_color -eq ""))) {
            $writeargs = @{ForegroundColor = "$my_response_color"}
        } Else {
            $writeargs = @{}
        }
        Write-Host "$print" @writeargs
    }

    Return $response
}

Function Xymon_MessageSummary() {
    param ($Message)
    $first_line = ($Message -split '\n')[0]
    Return $first_line
}


Function XymonCommunicateConfirm {
    Param ($Message)
    $first_line = Xymon_MessageSummary $Message
    $response = XymonCommunicate $Message
    Write-Host "Sent message to hobbit. Summary: $first_line"
    Return $response
}


# some of the versions of powershell that we need to target are so old
# that they don't support custom classes
# so we'll have to use global state instead

Function XymonReportCreate {
    Param(
    [string]$TestName,
    [string]$CurrentColor = 'clear',
    [string]$TTL,
    [string]$ReportHost,
    [string]$Group,
    [DateTime]$ReportCreatedWhen = (Get-Date),
    [string[]]$Lines = @()
    )

    # set these as globals
    $Global:TestName = $TestName
    $Global:CurrentColor = $CurrentColor
    $Global:TTL = $TTL
    $Global:ReportHost = $ReportHost
    $Global:Group = $Group
    $Global:ReportCreatedWhen = $ReportCreatedWhen
    $Global:Lines = $Lines

    XymonReportPrint("Chemistry Department - tool for auditing unmanaged computers`n`n")
}

Function XymonReportPrint {
    Param ($Line)
    $Global:Lines += $line
}

Function XymonReport_Color_Print {
    Param ($color, $line)
    XymonReport_add_color $color
    XymonReportPrint $line
}

Function XymonReport_Color_Line {
    Param ($color, $line)
    $msg = '&'+$color+' '+$line
    XymonReport_color_print $color $msg
}

Function XymonReport_PrepareMessage {
    $ttl_msg = If ($TTL) { '+' + $TTL } Else { '' }
    $group_msg = If ($Group) { '/group:' + $Group } Else { '' }
    $date_msg = Get-Date $ReportCreatedWhen -Format 'o'
    $ok_msg = If ($CurrentColor -eq 'green') { 'OK' } Else { 'Not OK' }
    $msg = "status${ttl_msg}${group_msg} $($ReportHost).$($TestName) $($CurrentColor) ${date_msg} - $ok_msg`n"
    $msg += $Lines -Join "`n"

    If ([string]::IsNullOrEmpty($ReportHost)) {
        throw "No hostname provided for test '$($TestName)'."
    }
    Return $msg
}

Function XymonReport_add_color($col) {
    $CurrentColor = max_color $col $CurrentColor
}

$allcolors = @{
    clear = 0
    green = 1
    purple = 2
    yellow = 3
    red = 4
}

Function max_color {
    param ($colA, $colB)
    If ($allcolors[$colA] -gt $allcolors[$colB]) {
        $ret = $colA
    } Else {
        $ret = $colB
    }
    Return $ret
}

Function min_color {
    param ($colA, $colB)
    If ($allcolors[$colA] -lt $allcolors[$colB]) {
        $ret = $colA
    } Else {
        $ret = $colB
    }
    Return $ret
}


##### end include


if (($help) -or ($h)) {
    $0 = $myInvocation.MyCommand.Definition
    # Get-Help won't work if this file is on a network drive!
    #Get-Help $0
    # So instead, manually pull out the content between the first set of documentation comment markers
    $help_text = (select-string -inputobject (Get-Content -Raw $0) -pattern "(?s)<#(.*?)#>").Matches.Groups[1].Value.Trim()
    Write-Host $help_text
    Exit 1
}

$available_sections = @("osver", "software", "updates")

if ($sections -eq "all") {
    $sections_ = $available_sections
} else {
    if (-not $sections) {
        Throw "no sections were specified"
    }
    $sections_ = $sections.Split(",")
}


ForEach ($section in $sections_) {
    If (-not ($available_sections -contains $section)) {
        Throw "Unknown section: $section"
    }
}

$hostname = $hostname.Trim()

Write-Host "System date/time is set to $(Get-Date -Format 'o')" -ForegroundColor yellow
Write-Host "Check that this is correct!" -ForegroundColor yellow

# don't use $my_ip_response.Headers.Date since that is in GMT, rather than the appropriate time zone

If ($prompt_verify_datetime) {
    Read-Host "Press ENTER once you have checked that the above date and time is roughly correct (or Ctrl-C to abort)"
}

Write-Host "PowerShell version:"
Try {
    $PSVersionTable
} Catch {
    # version 1.x doesn't have $PSVersionTable
    $Host.Version
}

Try {
    $system_uuid = (Get-WmiObject -Class Win32_ComputerSystemProduct).UUID
} Catch {
    $system_uuid = $Null
    Write-Host "Couldn't get system/SMBIOS UUID using WMI. Error was ""$($_.Exception.Message)"" [$($_.Exception.GetType().FullName)]" -ForegroundColor Red
}


$os_ver = Get-WmiObject -Class Win32_OperatingSystem
Write-Host "Windows Version: $($os_ver.Caption) - $($os_ver.Version)"

Write-Host "=== First step: identify the machine ==="
Write-Host "In case the system cannot be automatically identified, the following information may be useful to cross-check against the Chemistry database."

Try {
    #Write-Host "System name:        $((Get-WmiObject Win32_ComputerSystem).Name)"  # another form of computer name!
    Write-Host "System manufacturer: $((Get-WmiObject Win32_ComputerSystem).Manufacturer)"
    Write-Host "System model:        $((Get-WmiObject Win32_ComputerSystem).Model)"
} Catch {
}
Try {
    Write-Host "Serial number:       $((Get-WmiObject Win32_BIOS).SerialNumber)"
} Catch {
}
Write-Host ""

Write-Host "Determining MAC address(es) for system network adapters..."

Try {
    $macs = Get-WmiObject win32_networkadapterconfiguration | select description, macaddress |where {$_.macaddress -ne $Null} | where {$_.description -notmatch "Virtual"}
    $macs | Format-Table
} Catch {
    Write-Host "Couldn't get MAC addresses using WMI. Error was ""$($_.Exception.Message)"" [$($_.Exception.GetType().FullName)]" -ForegroundColor Red
    $macs = ipconfig /all
    Write-Host "MAC addresses can be checked with ipconfig /all"
}

Write-Host "This machine calls itself (N.B. this source of information is not reliable and is provided for information only):" -ForegroundColor Yellow
Write-Host " * $env:computername (from COMPUTERNAME environment variable)" -ForegroundColor Yellow
Try {
    Write-Host " *" (Get-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters' |% { $_.Hostname, $_.DhcpDomain -join '.' }) "(from Tcpip in registry)" -ForegroundColor Yellow
} Catch {}

Write-Host ""

$hostname_specified = $False
if ((($hostname -eq '') -or ($hostname -eq $null)) -and $allow_my_ip_lookup) {
    Try {
        Write-Host "Querying IP lookup service ($IP_lookup_service)..."
        $my_ip_response_obj = (wget https://$IP_lookup_service -UseBasicParsing -MaximumRedirection 0)
        $my_ip_response = $my_ip_response_obj.Content

        if ($my_ip_response -match "^Your machine's IP address is (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\. Its name is (.+)\s*$") {
            $hostname = $Matches.Item(2)
            if (-not (($hostname -eq '') -or ($hostname -eq $null))) {
                Write-Host "According to $IP_lookup_service, your machine's FQDN is $hostname" -ForegroundColor Green
            } Else {
                Write-Host "Null hostname provided by $IP_lookup_service" -ForegroundColor Red
            }
        } Else {
            Write-Host "Couldn't interpret the response from the IP lookup service whilst trying to determine the FQDN. Response was ""$my_ip_response""" -ForegroundColor Red
        }
    } Catch { # e.g. System.Net.WebException
        Write-Host "Couldn't contact the IP lookup service to determine the FQDN. Error was ""$($_.Exception.Message)"" [$($_.Exception.GetType().FullName)]" -ForegroundColor Red
    }
} ElseIf (!($hostname -Eq $Null) -And !($hostname -Eq "")) {
    Write-Host "Using specified hostname: $hostname" -ForegroundColor green
    $hostname_specified = $True
}

if ($hostname -like "*vpn*.ch.cam.ac.uk") {
    Write-Host "It appears that you are connected to the Chemistry VPN." -ForegroundColor yellow
    Write-Host "This tool cannot currently detect your hostname over a VPN, sorry." -ForegroundColor Red
    $hostname = ""
} ElseIf ($hostname -eq "legacy-os-rtr.ch.cam.ac.uk") {
    # actually this won't happen since the legacy OS VLAN can't reach the myip service!
    Write-Host "It appears that this machine is on the legacy OS VLAN." -ForegroundColor yellow
    Write-Host "This tool cannot currently detect your hostname from this VLAN, sorry." -ForegroundColor Red
    $hostname = ""
} ElseIf ($hostname -like "*chemnet*.ch.cam.ac.uk") {
    Write-Host "It appears that you are connected to the ChemNet gateway." -ForegroundColor yellow
    Write-Host "This tool cannot currently detect your hostname over the gateway, sorry." -ForegroundColor Red
    $hostname = ""
}

if (($hostname -eq '') -or ($hostname -eq $null)) {
    If ($allow_my_ip_lookup) {
        # already output something like this above
        #Write-Host "Couldn't determine your machine's FQDN via the myip service. Response was: ""$my_ip_response""" -ForegroundColor Red
    } Else {
        Write-Host "No FQDN specified and I was asked not to contact the myip service." -ForegroundColor red
    }
    If (-Not $save_offline) {
        Write-Host "Please either type the FQDN below, or leave blank to generate an offline report." -ForegroundColor Red
        $hostname = (read-host -Prompt "Please enter the hostname (as a FQDN)").Trim()
        $hostname_specified = $True
    }
}

if (($hostname -eq '') -or ($hostname -eq $null)) {
    $save_offline = $True
    Write-Host "Report will be saved offline."
    $do_xymon_check = $False
}

$Global:Hostname = $hostname
$global:FailedSend = $False
$save_base = $Null

# If we got this far, we hopefully have a guess of the hostname.
If ($do_xymon_check) {
    # Validate it.
    Write-Host "Checking host '$hostname' is known to hobbit by querying conn dot..."

    # Check that there is a 'conn' entry in xymon for this hostname.
    # Otherwise the report will be silently lost, with only an entry in the ghost clients page.
    # (Doesn't actually matter whether the host is reachable from hobbit though; we ignore the test result.)

    $Message = "query $hostname.conn"
    $xymon_response = $Null
    Try {
        $xymon_response = XymonCommunicateDebug $Message
    } Catch [System.Exception] {
        Write-Host "Error communicating with hobbit: $($_.Exception.Message)"
    }

    if (($xymon_response -eq $null) -or ($xymon_response.length -eq 0)) {
        Write-Host "Got an empty response from xymon, so either we have no network connectivity to xymon," -ForegroundColor yellow
        Write-Host "or this machine is not known to xymon." -ForegroundColor yellow
        Write-Host "Either way, I'll carry on by writing the output to files." -ForegroundColor yellow
        $hostname = $Null
        $save_offline = $True
        $ReportHostname = $system_uuid

    } else {
        Write-Host ""
        Write-Host "This script should have everything it needs to run and report to xymon!" -BackgroundColor darkgreen
        Write-Host ""
        $ReportHostname = $Hostname
        if (-Not (($hostname -eq '') -or ($hostname -eq $null))) {
            Write-Host "You therefore have two options:"
            Write-Host "1. report to xymon as $ReportHostname, or"
            Write-Host "2. save report offline (e.g. to USB stick) as $system_uuid"
            $choice = (read-host -Prompt "Choose 1 or 2 and press ENTER").Trim()
            If ($choice -eq "1") {
                # nothing to do here
                Write-Host "Okay, reporting online as $ReportHostname"
            } ElseIf ($choice -eq "2") {
                $hostname = $Null
                $save_offline = $True
                $ReportHostname = $system_uuid
                Write-Host "Okay, reporting offline as $ReportHostname"
            } Else {
                Write-Host "Didn't understand your selection. Aborting." -ForegroundColor red
                Exit 1
            }
        }
    }
} Else {
    Write-Host ""
    $ReportHostname = $system_uuid
}

function SaveFileDialog([string] $initialDirectory = "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}", [string] $initialName = "") {
    [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

    $FileDialog = New-Object System.Windows.Forms.SaveFileDialog
    $FileDialog.initialDirectory = $initialDirectory
    $FileDialog.filter = "All files (*.*)| *.*"
    $FileDialog.FileName = $initialName
    # script may hang without showing the dialog on some .NET versions without the below!?
    $FileDialog.ShowHelp = $True
    $FileDialog.OverwritePrompt = $True
    $FileDialog.CreatePrompt = $False
    #
    $result = $FileDialog.ShowDialog()

    If ($result -eq [System.Windows.Forms.DialogResult]::OK) {
        return $FileDialog.filename
    }
    return $Null
}

Function doOfflineSave {
    $info | ConvertTo-Json | Out-File -FilePath $save_base
    $msg = "Saved to $save_base"
    Write-Host "$msg"
}


# Now find out where to save the results
If ($save_offline) {
    Write-Host "You'll be asked to choose a save location next."

    Write-Host "If you have any problem with the save dialog or prefer not to use it, enter the path at the following prompt."
    Write-Host "Otherwise, just leave the following blank at the prompt to see a save dialog."
    Write-Host "e.g. you might type E:\{ReportHostname}.json if E: is the name of your USB drive (you must check this yourself)"
    Write-Host "{ReportHostname} will be automatically replaced with $system_uuid"
    Write-Host "If you just specify a drive letter, e.g. E"
    Write-Host "then it will save as E:\{YYYY-MM-DD}\{ReportHostname}.json without you having to type it all out;"
    Write-Host "in that case (and only then) it will also ensure that the dated folder exists."
    Write-Host ""

    $save_base = (Read-Host -Prompt "Enter a save location or just press ENTER to see a save as dialog").Trim()
    If (($save_base -eq "") -Or ($save_base -eq $Null)) {
        $save_base = $Null
    } Else {
        If ($save_base.Length -eq 1) {
            $save_base = "$($save_base):\$(Get-Date -Format yyyy-MM-dd)"
            # ensure the directory exists (without erroring if it's already there)
            MkDir "$save_base" -ErrorAction SilentlyContinue
            # append the filename
            $save_base += "\" + $ReportHostname + ".json"
        } Else {
            $save_base = $save_base.Replace("{YYYY-MM-DD}", $(Get-Date -Format yyyy-MM-dd)).Replace("{ReportHostname}", $ReportHostname)
        }
    }

    If ($save_base -eq $Null) {
        # prompt for a filename (or basename)
        $opts = @{initialName = ($system_uuid + ".json")}
        Write-Host "Where should I save to? See 'Save As' dialog box (it may not have the focus)..."
        $save_base = SaveFileDialog @opts
    }

    If ($save_base -Eq $Null) {
        Write-Host "No filename selected. Aborting." -ForegroundColor red
        Exit 1
    }
    Write-Host "Will write reports to $save_base"

    # start by populating with all the information we've gathered already
    $info = @{os_ver = $os_ver; macs = $macs}

    Try {
        $info['PSVersionTable'] = $PSVersionTable
    } Catch {
    }
    $info['Host_Version'] = $Host.Version

    $info["Env_ComputerName"] = $Env:ComputerName
    $info["System_UUID"] = $system_uuid

    Try {
        $info['Script_ExecutedFrom'] = $MyInvocation.MyCommand.Path
    } Catch {
    }
    Try {
        $info['Script_LastWriteTime'] = (Get-Item $MyInvocation.MyCommand.Path).LastWriteTime.ToString("yyyy-MM-dd HH:mm:ss")
    } Catch {
    }
    Try {
        $info['Script_hash'] = (CertUtil -hashfile "$($MyInvocation.MyCommand.Path)" MD5)
    } Catch {
    }
    Try {
        $info["Manufacturer"] = (Get-WmiObject Win32_ComputerSystem).Manufacturer
    } Catch {
    }
    Try {
        $info["Model"] = (Get-WmiObject Win32_ComputerSystem).Model
    } Catch {
    }
    Try {
        $info["Serial_Number"] = (Get-WmiObject Win32_BIOS).SerialNumber
    } Catch {
    }

    #$info | ConvertTo-JSON

    doOfflineSave
}



Write-Host "=== Second step: run audits ==="

Write-Host "The following audit(s) have been configured: $($sections_ -Join ',')."

Function MyXymonReport_Send {
    If ($no_submit) {
        Return
    } Else {
        $msg = XymonReport_PrepareMessage
        If ($show_all) {
            XymonCommunicateDebug $msg
        } Else {
            XymonCommunicateConfirm $msg
        }
    }
}

Function Section_osver {
    Write-Host ""
    Write-Host "Operating system"
    Write-Host "----------------"

    $os_ver = Get-WmiObject -Class Win32_OperatingSystem

    $report_state = @{
        TestName = 'osver';
        CurrentColor = 'green';
        TTL = '52w';
        ReportHost = $ReportHostname
    }
    XymonReportCreate @report_state
    XymonReportPrint $os_ver.Caption
    XymonReportPrint "Version $($os_ver.Version)"

    If (-Not $save_offline) {
        MyXymonReport_Send
    } Else {
        # info object already contains the requested information!
        # but add xymon message anyway
        $info['report_osver'] = XymonReport_PrepareMessage
        $summary = Xymon_MessageSummary($info['report_osver'])
        Write-Host $summary
        doOfflineSave
    }
}

Function Section_software {
    Write-Host ""
    Write-Host "Installed software"
    Write-Host "------------------"

    $report_state = @{
        TestName = 'software';
        CurrentColor = 'green';
        TTL = '52w';
        ReportHost = $ReportHostname
    }
    XymonReportCreate @report_state

    function doKey($base) {
        Write-Host "Searching in $base"
        $SubKeys = (Get-ChildItem -Path $base)
        Foreach ($SubKey in $SubKeys) {
            $DisplayName = $SubKey.GetValue("DisplayName")
            #$DisplayName = Get-ItemProperty ($base+'\\'+$SubKey) -Name 'DisplayName'
            if (($DisplayName -ne $null) -and (-not ($DisplayName -match "^(Definition )?(Security )?Update for Microsoft "))) {
                XymonReportPrint("Display Name: $DisplayName")
                $DisplayVersion = $SubKey.GetValue('DisplayVersion')
                $InstallDate = $SubKey.GetValue('InstallDate')
                if ($DisplayVersion -ne $null) {
                    XymonReportPrint("  DisplayVersion: $DisplayVersion");
                    # name+version, just to provide an easily bb-greppable string
                    # some DisplayNames include the DisplayVersion so the output may look
                    # repetitious in some cases. Meh.
                    XymonReportPrint("  DisplayName+DisplayVersion: $DisplayName $DisplayVersion")
                }
                if ($InstallDate -ne $null) {
                    XymonReportPrint("  Install Date: $InstallDate")
                }
                XymonReportPrint('=======================================================')
            }
        }
    }
    doKey Registry::HKLM\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall
    doKey Registry::HKLM\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall

    If (-Not $save_offline) {
        MyXymonReport_Send
    } Else {
        $info['report_software'] = XymonReport_PrepareMessage
        $summary = Xymon_MessageSummary($info['report_software'])
        Write-Host $summary
        doOfflineSave
    }
}

Function Section_updates {
    Write-Host ""
    Write-Host "Updates"
    Write-Host "-------"

    Write-Host "Creating Com Object..."
    $updateSession = New-Object -ComObject "Microsoft.Update.Session"
    Write-Host "Creating update searcher..."
    $updateSearcher = $updateSession.CreateUpdateSearcher()

    $report_state = @{
        TestName = 'updates';
        CurrentColor = 'green';
        TTL = '52w';
        ReportHost = $ReportHostname
    }
    XymonReportCreate @report_state

    Write-Host "Searching for updates..."
    $Failed = $False
    Try {
        $searchResult = $updateSearcher.Search("IsInstalled=0 and IsHidden=0")
    } Catch {
        Write-Host "Couldn't search for updates. Error was ""$($_.Exception.Message)"" [$($_.Exception.GetType().FullName)]" -ForegroundColor Red
        XymonReport_color_line "clear" "Failed to search for updates. Error was ""$($_.Exception.Message)"" [$($_.Exception.GetType().FullName)]"
        $Failed = $True
    }

    If (-Not $Failed) {
        Write-Host "Processing results..."
        $update_age_thresholds = @{}
        $update_age_thresholds.Critical = @{
            overdue_age = 7
            overdue_color = 'red'
            normal_color = 'yellow'}
        $update_age_thresholds.Moderate = @{
            overdue_age = 21
            overdue_color = 'yellow'
            normal_color = 'green'}
        $update_age_thresholds.Low = @{
            overdue_age = 28
            overdue_color = 'yellow'
            normal_color = 'green'}
        $update_age_thresholds.Important = @{
            overdue_age = 14
            overdue_color = 'yellow'
            normal_color = 'green'}

        XymonReportPrint "$($searchResult.Updates.Count) update(s) is/are available"
        Foreach($update in $searchResult.Updates) {
            $t = $update.Title
            $l = $update.LastDeploymentChangeTime
            $displayTime = $l.ToString((Get-Culture).DateTimeFormat.RFC1123Pattern)
            $delay = New-TimeSpan -Start $l -End (Get-Date)

            If (($update.MsrcSeverity -ne $null) -and ($update_age_thresholds.ContainsKey($update.MsrcSeverity))) {
                $effective_severity = $update.MsrcSeverity
            } else {
                $effective_severity = "Important"
            }
            $thresholds = $update_age_thresholds[$effective_severity]
            If ($delay -gt (New-TimeSpan -Days $thresholds.overdue_age)) {
                $color = $thresholds.overdue_color
            } Else {
                $color = $thresholds.normal_color
            }
            XymonReport_color_line $color "$t [$effective_severity; released $displayTime]"
        }
    }
    If (-Not $save_offline) {
        MyXymonReport_Send
    } Else {
        $info['report_updates'] = XymonReport_PrepareMessage
        $summary = Xymon_MessageSummary($info['report_updates'])
        Write-Host $summary
        doOfflineSave
    }
}


ForEach ($section in $sections_) {
    Invoke-Expression Section_$section
}

Write-Host ""
Write-Host ""

If ($save_offline) {
    doOfflineSave

    $msg = "Saved to $save_base"

    Write-Host "Showing confirmation dialog (may not have focus)..."

    Add-Type -AssemblyName PresentationFramework
    [System.Windows.MessageBox]::Show("$msg", 'Chemistry department', 'OK', 'Information') | Out-Null
} Else {
    If ($no_submit) {
        $sent_verb = "echoed"
        $send_verb = "echo"
    } Else {
        $sent_verb = "sent"
        $send_verb = "send"
    }
    If (!$Global:FailedSend) {
        Write-Host "----------------" -ForegroundColor green
        Write-Host "All done: $sent_verb report(s) $($sections_ -Join ",") for $hostname" -ForegroundColor green
        Write-Host "----------------" -ForegroundColor green

        If (-not $no_submit) {
            Add-Type -AssemblyName PresentationFramework
            [System.Windows.MessageBox]::Show("Report(s) $sent_verb for $hostname for $($sections_ -Join ",").", 'Chemistry department', 'OK', 'Information') | Out-Null
        }
    } Else {
        Write-Host "----------------" -ForegroundColor red
        Write-Host "Done, but there were errors" -ForegroundColor red
        $msg = "Attempted to $send_verb $($sections_ -Join ",") report(s) for $hostname"
        Write-Host $msg -ForegroundColor red
        Write-Host "----------------" -ForegroundColor red

        If (-not $no_submit) {
            Add-Type -AssemblyName PresentationFramework
            [System.Windows.MessageBox]::Show("$msg, but there were errors.", 'Chemistry department', 'OK', 'Information') | Out-Null
        }
    }
}
