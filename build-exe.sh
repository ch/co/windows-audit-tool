#!/bin/bash
set -e

if ! command -v 7zr &> /dev/null
then
    echo "7zr could not be found; try sudo apt-get install p7zip"
    exit
fi

SOURCE_DIR=source
DEST_EXE=audit.exe
SFX=7z-extra/7zSD.sfx

TEMPZIP=$(mktemp --suffix=.7z -u)

# Create a zipfile
cd "${SOURCE_DIR}"
7zr a "${TEMPZIP}" './'
cd ..

# Create the self extracting exe
cat "${SFX}" config.txt "${TEMPZIP}" > "${DEST_EXE}"

# Remove tmpfile
if [ -f "${TEMPZIP}" ];then
  rm "${TEMPZIP}"
fi
